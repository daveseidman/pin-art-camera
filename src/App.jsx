import React, { useEffect, useRef, useState } from 'react';
import { MeshStandardMaterial, InstancedMesh, CylinderGeometry, Vector3, Matrix4, MeshDepthMaterial, BackSide } from 'three';
import { Canvas, useFrame } from '@react-three/fiber';
import { Environment, OrbitControls, PerspectiveCamera, OrthographicCamera, useGLTF } from '@react-three/drei';
import { FaceMesh } from '@mediapipe/face_mesh';
import { Camera } from '@mediapipe/camera_utils';
import tvStudio from './assets/images/studio_small_08_1k.hdr';
import videoFile from './assets/videos/test1.mp4';
import faceModel from './assets/models/face-3.gltf';
import './index.scss';

const getEyeDistance = (landmarks) => {
  const leftEyeIndex = 468;
  const rightEyeIndex = 473;
  const leftEyePosition = new Vector3(landmarks[leftEyeIndex].x, landmarks[leftEyeIndex].y, landmarks[leftEyeIndex].z);
  const rightEyePosition = new Vector3(landmarks[rightEyeIndex].x, landmarks[rightEyeIndex].y, landmarks[rightEyeIndex].z);

  return leftEyePosition.distanceTo(rightEyePosition);
};

function Face({ landmarks, drawToCanvas, width, height, setUseFace }) {
  const faceRef = useRef();
  const { nodes } = useGLTF(faceModel);
  const obj = nodes.FaceMesh;
  obj.material = new MeshDepthMaterial({ side: BackSide });
  const { position } = obj.geometry.attributes;
  const eyeDistance = useRef(0);
  const scaleOffset = 0.25;
  const smooth = 2;
  const fadeIn = useRef(0);
  const magnitude = 100;

  const meshPosition = [0, 0, 0];

  if (landmarks && landmarks.length) {
    fadeIn.current += 1;
    setUseFace(true);

    landmarks.forEach((landmark, index) => {
      const { x, y, z } = landmark;
      position.array[(index * 3) + 0] += (x - position.array[(index * 3) + 0]) / smooth;
      position.array[(index * 3) + 1] += (y - position.array[(index * 3) + 1]) / smooth;
      position.array[(index * 3) + 2] += (z - position.array[(index * 3) + 2]) / smooth;
    });

    eyeDistance.current = getEyeDistance(landmarks);

    meshPosition[0] += (((-scaleOffset / 2) / eyeDistance.current) - meshPosition[0]) / smooth;
    meshPosition[2] += (((-scaleOffset / 2) / eyeDistance.current) - meshPosition[2]) / smooth;

    position.needsUpdate = true;
    if (fadeIn.current > 50) drawToCanvas();
  }

  return (
    <group rotation={[Math.PI / 2, 0, 0]}>
      <primitive
        position={meshPosition}
        ref={faceRef}
        object={obj}
        scale={[scaleOffset / eyeDistance.current, scaleOffset / eyeDistance.current, magnitude]}
      />
    </group>
  );
}

const useForceRender = () => {
  const [, forceRender] = useState();
  return () => forceRender((prevState) => !prevState);
};

function Pins({ rows, cols, video, face, useFace }) {
  const meshRef = useRef();
  const spacing = 0.33;
  const magnitude = 100;
  const count = rows * cols;
  const forceRender = useForceRender();
  const width = 64;
  const height = 64;
  const canvas = useRef();
  const context = useRef();

  const instancedMesh = new InstancedMesh(
    new CylinderGeometry(0.1, 0.1, 6, 12),
    new MeshStandardMaterial({ color: 0x333333, roughness: 0.15, metalness: 0.6, envMapIntensity: 4 }),
    count,
  );

  // Position instances at regular intervals
  // TODO: maybe put this in the useEffect so it's only set once?
  let index = 0;
  for (let x = 0; x < cols; x += 1) {
    for (let z = 0; z < rows; z += 1) {
      instancedMesh.setMatrixAt(index, new Matrix4().makeTranslation((x - (cols / 2)) * spacing, 0, (z - (rows / 2)) * spacing));
      index += 1;
    }
  }

  instancedMesh.instanceMatrix.needsUpdate = true;

  useFrame(() => {
    context.current.drawImage(useFace ? face.current : video.current, 0, 0, width, height);
    const imageData = context.current.getImageData(0, 0, width, height);
    let index = 0;
    for (let x = 0; x < cols; x += 1) {
      for (let z = 0; z < rows; z += 1) {
        const offsetX = Math.floor((x / cols) * width);
        const offsetY = Math.floor((z / rows) * height);
        const position = ((offsetY * rows) + offsetX) * 4;
        const r = imageData.data[position + 0];
        const g = imageData.data[position + 1];
        const b = imageData.data[position + 2];
        const total = r + g + b;
        instancedMesh.setMatrixAt(index, new Matrix4().makeTranslation((x - (cols / 2)) * spacing, total / magnitude, (z - (rows / 2)) * spacing));
        // instancedMesh.needsUpdate = true;
        index += 1;
      }
    }
    forceRender();
  });

  useEffect(() => {
    if (!canvas.current) {
      canvas.current = document.createElement('canvas');
      canvas.current.width = width;
      canvas.current.height = height;
      context.current = canvas.current.getContext('2d');
    }
  }, []);

  return (
    <primitive
      object={instancedMesh}
      ref={meshRef}
      castShadow
      receiveShadow
      visible
    />
  );
}

function App() {
  const width = 640;
  const height = 640;
  const [landmarks, setLandmarks] = useState([]);
  const [cameraStarted, setCameraStarted] = useState(false);
  const [useFace, setUseFace] = useState(false);
  const videoRef = useRef();
  const sourceRef = useRef();
  const canvas1Ref = useRef();
  const canvas2Ref = useRef();
  const camera = useRef();

  const drawToCanvas = () => {
    canvas2Ref.current.getContext('2d').drawImage(canvas1Ref.current, 0, 0);
  };

  // console.log(cameraRef.current.renderTarget);
  const onResults = (results) => {
    setLandmarks(results.multiFaceLandmarks[0]);
  };

  const faceMesh = new FaceMesh({ locateFile: (file) => `https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh/${file}` });
  faceMesh.setOptions({
    maxNumFaces: 1,
    refineLandmarks: true,
    minDetectionConfidence: 0.5,
    minTrackingConfidence: 0.5,
  });
  faceMesh.onResults(onResults);

  const toggleCamera = () => {
    if (cameraStarted) {
      // video.pause();
      camera.current.stop().then((err) => {
        console.log(err);
        if (!err) setCameraStarted(false);
      });
    } else {
      camera.current.start().then((err) => {
        if (!err) setCameraStarted(true);
      });
    }
  };

  const clearCanvas = () => {
    const context = canvas2Ref.current.getContext('2d');
    canvas2Ref.current.getContext('2d').globalCompositeOperation = 'source-over';
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);
    canvas2Ref.current.getContext('2d').globalCompositeOperation = 'lighten';
    // drawToCanvas(); // TODO: not working yet
  };

  useEffect(() => {
    camera.current = new Camera(sourceRef.current, {
      onFrame: async () => {
        await faceMesh.send({ image: sourceRef.current });
      },
    });
    canvas2Ref.current.getContext('2d').globalCompositeOperation = 'lighten';
  }, []);

  return (
    <div className="app">
      <Canvas
        ref={canvas1Ref}
        className="canvas1"
        dpr={1}
        shadows
        width={width}
        height={height}
        gl={{ preserveDrawingBuffer: true }}
        style={{ width: `${width}px`, height: `${height}px` }}
      >
        <OrthographicCamera
          makeDefault
          zoom={1}
          top={0}
          bottom={-1}
          left={1}
          right={0}
          near={1}
          far={15}
          position={[0, 0, 10]}
        />
        <Face
          landmarks={landmarks}
          drawToCanvas={drawToCanvas}
          width={width}
          height={height}
          setUseFace={setUseFace}
        />
      </Canvas>
      <canvas
        ref={canvas2Ref}
        className="canvas2"
        width={width}
        height={height}
        style={{ width: `${width}px`, height: `${height}px` }}
      />
      <div className="source">
        <video
          ref={sourceRef}
          muted
          autoPlay
          playsInline
        />
      </div>
      <video
        width={width}
        height={height}
        ref={videoRef}
        className="vid"
        src={videoFile}
        muted
        loop
        autoPlay
        playsInline
      />
      <Canvas
        className="final"
        style={{ width: '640px', height: '640px' }}
      >
        <PerspectiveCamera
          makeDefault
          position={[0, 20, 20]}
        />
        <Pins
          rows={64}
          cols={64}
          face={canvas2Ref}
          video={videoRef}
          useFace={useFace}
        />
        <Environment
          files={tvStudio}
          blur={0.2}
          background
        />
        <directionalLight
          intensity={1}
          castShadow
          position={[0, 2, 0]}
          target-position={[1, 0, 0]}
        />
        <OrbitControls />
      </Canvas>
      <div className="controls">
        <button
          type="button"
          onClick={toggleCamera}
        >
          {cameraStarted ? 'Stop' : 'Start'}
        </button>
        <button
          type="button"
          onClick={clearCanvas}
        >
          Clear
        </button>
      </div>
    </div>
  );
}

export default App;
