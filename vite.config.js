import react from '@vitejs/plugin-react';

import { defineConfig } from 'vite';

export default defineConfig({
  plugins: [
    react(),
  ],
  assetsInclude: ['**/*.obj', '**/*.gltf', '**/*.glb', '**/*.hdr'],
  server: {
    port: 8080,
  },
});
